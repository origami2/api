var debug = require('debug');

function APIClient(stackClient) {
  this.stackClient = stackClient;
}

var shadow = require('./shadow');

function APIClientBuilder() {
}

APIClientBuilder.prototype.build = function (stack, fold) {
  return new Promise(function (resolve, reject) {
    var stackClient = require('socket.io-client')(stack);

    stackClient.on('connect', function () {
      stackClient.on('identify', function (callback) {
        callback('fold', fold);

        stackClient.emit('describe-apis', function (err, apis) {
          if (err) {
            stackClient.disconnect();
            return reject(err);
          }

          var client = new APIClient();

          for (var apiName in apis) {
            var methods = apis[apiName];

            var apiConstructor = new shadow.Constructor(
              apiName,
              apis[apiName],
              function (apiName, methodName) {
                return shadow.renameFunction(
                  methodName,
                  methods[methodName],
                  function () {
                    var apiDebug = debug('origami:api:client:' + apiName);
                    var params = {};

                    for (var i = 0; i < apis[apiName][methodName].length; i++) {
                      var paramName = apis[apiName][methodName][i];

                      params[paramName] = arguments[i];
                    }

                    return new Promise(function (resolve, reject) {
                      apiDebug('.%s(%s)', methodName, JSON.stringify(params));

                      stackClient.emit('api', fold, apiName, methodName, params, function (err, result) {
                        if (err) {
                          apiDebug('.%s error: %s', methodName, err);
                          return reject(err);
                        }

                        apiDebug('.%s result: %s', JSON.stringify(result));

                        resolve(result);
                      });
                    });
                  }
                );
              }
            );

            client[apiName] = new apiConstructor();
          }

          resolve(client);
        });
      });
    });
  });
}

module.exports = APIClientBuilder;
