var Shadow = require('./shadow');

function PluginDependency(name) {
  this.name = name;
}

function callbackFor(resolve, reject) {
  return function (err, result) {
    if (err) return reject(err);

    resolve(result);
  };
}

PluginDependency.prototype.build = function (stackClient, fold) {
  var self = this;

  return new Promise(function (resolve, reject) {
    var debug = require('debug')('origami:PluginDependency:' + self.name + ':' + fold);

    stackClient.emit('describe-api', self.name, function (err, methods) {
      if (err) return reject(err);

      try {
        var cons = new Shadow.Constructor(
          self.name,
          methods,
          function (apiName, methodName) {
            return Shadow.renameFunction(methodName, methods[methodName], function () {
              var args = [];
              var argumentNames = methods[methodName];

              for (var i = 0; i < arguments.length; i++) {
                args.push(arguments[i]);
              }

              debug('invoke ' + apiName + '.' + methodName + '(' + JSON.stringify(args) + ')');

              return new Promise(function (resolve, reject) {
                var params = {};

                for (var i = 0; i < args.length; i++) {
                  params[argumentNames[i]] = args[i];
                }

                stackClient.emit('api', fold, apiName, methodName, params, callbackFor(resolve, reject));
              });
            });
          }
        );

        var target = Object.create(cons.prototype);

        resolve(target);
      } catch (e) {
        return reject(e);
      }
    });
  })
};

module.exports = PluginDependency;
