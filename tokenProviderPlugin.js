function TokenProviderPlugin(api, locals, pluginPrivateKey, stackPublicKey) {
  if (typeof(api) !== 'function') {
    throw new Error('API must be a constructor function')
  }

  if (!pluginPrivateKey) {
    throw new Error('Private key must be provided for a Token Provider');
  }

  if (!stackPublicKey) {
    throw new Error('Private key must be provided for a Token Provider');
  }

  if (!this.api.prototype.getToken) {
    throw new Error('API must have a method .getToken');
  }

  this.api = api;
  this.locals = locals;

  this.privateKey = new NodeRSA();
  this.privateKey.importKey(pluginPrivateKey);

  this.stackKey = new NodeRSA();
  this.stackKey.importKey(stackPublicKey);
}

TokenProviderPlugin.prototype.connect = function (server, privateKey) {
  var self = this;

  return new Promise(function (resolve, reject) {
    var stackClient = require('socket.io-client')(server, {
      timeout: 5000,
      reconnectionAttempts: 1
    });

    stackClient.on('connect_timeout', function () {
      reject('cannot connect to the stack');
    });

    stackClient.on('connect', function () {
      self.debug('connected to the stack');

      stackClient.on('identify', function (callback) {
        callback('token-provider', self.api.name)
      });

      stackClient.on('challenge-sign-encrypt', function (input, callback) {
        try
        {
          var responseObject = {
            data: input,
            signature: self.privateKey.sign(input)
          };

          var responseClear = JSON.stringify(responseObject);

          var output = self.stackKey.encrypt(responseClear, 'base64');

          callback(null, output);
        } catch (e) {
          callback(e);
        }
      });
    });
  });
};

TokenProviderPlugin.prototype.prototype = require('./plugin');
