function renameFunction (name, args, fn) {
  return (new Function("return function (call) { return function " + name +
      " ("+ args.join(',') +") { return call(this, arguments) }; };")())(Function.apply.bind(fn));
};

function ShadowMethod(methodName, argNames, implementation) {
  var method = function () {
    var args = [];

    for (var i = 0; i < arguments.length; i++) { args.push(arguments[i]); }

    return implementation.apply(this, args);
  };

  return renameFunction(methodName, argNames, method);
}

function ShadowConstructor (apiName, methods, methodBuilder) {
  var cons = new Function ();

  cons = renameFunction(apiName, [], cons);

  cons.prototype = {
  };

  for (var methodName in methods) {
    cons.prototype[methodName] = new ShadowMethod(methodName, methods[methodName], methodBuilder(apiName, methodName));
  }

  return cons;
}

module.exports = {
  Constructor: ShadowConstructor,
  Method: ShadowMethod,
  renameFunction: renameFunction
}
