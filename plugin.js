var async = require('async');

var PluginDependency = require('./dependency');
var Shadow = require('./shadow');

function getMethodArguments(fn) {
  var FN_ARGS = /^function\s*[^\(]*\(\s*([^\)]*)\)/m;
  var FN_ARG_SPLIT = /,/;
  var FN_ARG = /^\s*(_?)(.+?)\1\s*$/;
  var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;

  if (typeof(fn) === 'function') fn = fn.toString();

  var argNames = [],
      fnText = fn.replace(STRIP_COMMENTS, ''),
      argDecl = fnText.match(FN_ARGS),
      splitted = argDecl[1].split(FN_ARG_SPLIT);

  for (var i = 0; i < splitted.length; i++) {
    var arg = splitted[i];

    arg.replace(FN_ARG, function(all, underscore, name){
      argNames.push(name);
    });
  }

  return argNames;
}

function getApiMethods(api) {
  var methods = {};

  for (var m in api) {
    if (typeof(api[m]) === 'function') methods[m] = getMethodArguments(api[m])
  }

  return methods;
}

function Plugin(api, locals) {
  if (typeof(api) !== 'function') throw new Error('API must be a function');
  if (!api.name) throw new Error ('API must not be an anonymous function');

  this.api = api;
  this.name = api.name;
  this.locals = locals || {};
  this.methods = getApiMethods(api.prototype);

  this.debug = require('debug')('origami:plugin:' + api.name);
}

Plugin.prototype.connect = function (server, callback) {
  var self = this;

  var stackClient = require('socket.io-client')(server, {
    timeout: 5000,
    reconnectionAttempts: 1
  });

  stackClient.on('connect_timeout', function () {
    callback('cannot connect to the stack');
  });

  stackClient.on('connect', function () {
    self.debug('connected to the stack');

    stackClient.on('identify', function (callback) {
      stackClient.on('api', function (fold, methodName, args, invokeCallback) {
        var constructorArgNames = getMethodArguments(self.api);
        var constructorArguments = [];

        var fixed = {
          'fold': fold,
          'methodInvoker': function (apiName, methodName, params) {
            return new Promise(function (resolve, reject) {
              stackClient.emit(
                'api',
                fold,
                apiName,
                methodName,
                params,
                function (err, result) {
                  if (err) return reject(err);

                  resolve(result);
                }
              );
            });
          },
          'announce': function () {
            self.debug('announce: %s', JSON.stringify(arguments));
            var args = ['announce', fold];

            for (var i = 0; i < arguments.length; i++) args.push(arguments[i]);

            stackClient.constructor.prototype.emit.apply(stackClient, args);
          }
        };

        async.mapSeries(constructorArgNames, function (argName, callback) {
          if (fixed[argName]) return callback(null, fixed[argName]);

          var local = self.locals[argName];

          if (local === undefined) return callback('Argument ' + argName + ' is not provided for ' + self.name);
          if (local === null) return callback(null, null);

          if (local.constructor === PluginDependency) {
            try {
              local.build(stackClient, fold)
              .then(function (target) {
                callback(null, target);
              }, function (err) {
                self.debug(err);
                callback(err);
              });
            } catch (e) {
              callback('Couldn\'t build argument ' + argName + ' for ' + self.name);
            }
          } else {
            callback(null, local);
          }
        },
        function (err, constructorArguments) {
          if (err) return invokeCallback(err);

          var constructor = self.api;
          var prototype = constructor.prototype;
          var instance = Object.create(prototype);

          try {
            self.debug('creating instance of %s for %s', self.api.name, fold);
            constructor.apply(instance, constructorArguments);
          } catch (e) {
            return invokeCallback (e);
          }

          var methodArguments = self.methods[methodName];
          var invokeArguments = [];

          for (var j = 0; j < methodArguments.length; j++) {
            var methodArgumentName = methodArguments[j];
            invokeArguments.push(args[methodArgumentName]);
          }

          invokeArguments.push(invokeCallback);

          self.debug('invoking %s(%s)', methodName, JSON.stringify(methodArguments));

          try {
            var promise = prototype[methodName].apply(instance, invokeArguments);

            promise.then(
              function (result) {
                self.debug('%s ok', methodName);
                invokeCallback(null, result);
              },
              function (err) {
                self.debug('%s error: %s', methodName, JSON.stringify(args), JSON.stringify(error));
                invokeCallback(err);
              }
            );
          } catch (e) {
            self.debug('%s(%s) error: %s', self.api.name, methodName, JSON.stringify(args), e.message || e);
            invokeCallback(e);
          }
        });
      });

      callback('plugin', self.name, self.methods);
    });
  });
};

Plugin.require = function (pluginName) {
  return new PluginDependency(pluginName);
};

module.exports = Plugin;
